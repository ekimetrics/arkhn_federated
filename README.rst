=======
EkiFhir
=======


.. image:: https://img.shields.io/pypi/v/EkiFhir.svg
        :target: https://pypi.python.org/pypi/EkiFhir

.. image:: https://img.shields.io/travis/romainledoux/EkiFhir.svg
        :target: https://travis-ci.com/romainledoux/EkiFhir

.. image:: https://readthedocs.org/projects/EkiFhir/badge/?version=latest
        :target: https://EkiFhir.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Transform user's query to FHIR REST API and parse the response to a Pandas Dataframe


* Free software: MIT license
* Documentation: https://EkiFhir.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
