# -*- coding: utf-8 -*-
import requests
import json

class RequestApi(object):

    """
    A class that dynamically requests a REST API given a URL

    ...

    Attributes
    ----------
    url: str
        the url we use to request an API
        Example of a URL to request a standard FHIR API:
        'http://hapi.fhir.org/baseR4/Observation?subject:Patient.birthdate=ge1970&code=http://loinc.org|55284-4&_include=Observation:subject'
        

    Methods
    -------
    request
        Returns a requests.models.Response:
         - response: retrieve a response from an API 

    response
        Returns a dictionary:
         - json_dictionnary: a nested JSON file returned by the API
    """
    
    def __init__(self, URL):
        
        self.URL = URL

    def request_api(self):

        """Send a request to a FHIR API given a url."""
        payload = {}
        headers= {}
        url = self.URL

        response = requests.request("GET", url, headers=headers, data = payload)

        print("URL Request : ", url, "\nResponse: ", response, "\n")
        
        return response


    def response(self):

        """Convert the API's response to a JSON dictionnary."""
        response = self.request_api()
        json = response.json()
        
        return json