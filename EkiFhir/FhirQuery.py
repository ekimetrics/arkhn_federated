# -*- coding: utf-8 -*-
import requests
import json
from EkiFhir.RequestApi import RequestApi
from EkiFhir.ToDataset import ToDataset


class FhirQuery(object):

    """   
    A class that dynamically creates a URL that respects the 
    search syntax for a standard FHIR API

    ...

    Attributes
    ----------
    path_to_query_file: str
        the absolute path to the JSON query_fileuration file
        Example of the structure of a JSON query_file file:
        {
        "select": {
            "Observation": [
            "component.valueQuantity.value"
            ],
            "Patient": [
            "birthDate",
            "gender"
            ]
        },
        "from": {
            "Patient": "patient",
            "Observation": "observation"
        },
        "join": {
            "Observation": {
            "subject": "patient"
            }
        },
        "where": {
            "Patient" : {
            "birthdate": {"ge": "1970"}
            },
            "Observation": {
            "code": "http://loinc.org|55284-4"
            }
        }
        }

    base: str
        the root of any search URL for a given API (e.g. "http://hapi.fhir.org/baseR4/" for HAPI FHIR)

    Methods
    -------

    """
     
    def __init__(self, base_url, path_to_query_file):

        self.base = base_url
        self.path = path_to_query_file
        self.query_file = self.read_json()
        self.source_resource = self.get_source_resource()

        self.from_res = self.get_from_res()
        self.join_res = self.get_join_res()
        self.where_res = self.get_filters()[0]

        self.related_res_filters = self.get_filters()[1]
        self.source_res_filters = self.get_filters()[2]
        self.related_res_filters_full = self.get_related_res_filters()
        self.source_res_filters_full = self.get_source_res_filters()
        
        self.include_parameter = self.include_res()

    def read_json(self):

        with open(self.path) as f:
            file = json.load(f)
            
        return file        
    
    def get_source_resource(self):
        """
        Return the resource that will be use as the source resource 
        (i.e. the resource taking place immediately after the base url + '?' in the API's query
        The source resource is the first resouce to appear in the FROM clause (SQL logic)
        """
                #         If there is more than one resource mentionned in the Select clause
                #         We fetched the first mentioned resource in the Join clause in order
        # The base resource is the first one mentionned in the FROM statement
        
        try:
            source_resource = list(self.query_file["from"].keys())[0].capitalize()
        except:
            print("Wrong syntax in FROM statement")
                    
        return source_resource
    
    def get_select_res(self):
        
        res = list(self.query_file["select"].keys())
        return res
    
    def get_from_res(self):
        
        try:
            res = list(self.query_file["from"].keys())
        except:
            res =''
        return res
    
    def get_join_res(self):
        
        try:
            res = self.query_file["join"]
        except:
            res =''
            
        return res
    
    def get_where_res(self):
        
        try:
            res = self.query_file["where"]
        except:
            res = []
        return res
    
    def get_filters(self):
        """
        Get filters from the Source resource and optional Related resources
        Input: Where clause
        Output:
            - Filters from Source Resource
            - Filters from Related Resources
            - Related Resources' Names
        """
        
        where_res = []
        source_res_filters  = []
        related_res_filters = []
               
        where_clause = self.get_where_res()
        res_src = self.source_resource.lower()

        # If WHERE statement not empty
        if len(where_clause) > 0:
            for i, key in enumerate(where_clause):
                # Filters from source resource
                if key == res_src:
                    source_res_filters.append(where_clause[key])
                    where_res.append(key)
                # Filters from related resources
                elif key != res_src:
                    related_res_filters.append(where_clause[key])
                    where_res.append(key)

        return where_res, related_res_filters, source_res_filters   
    

    def get_related_res_filters(self):
        """
        Build URL syntax for Related Resources' filters
        Input: 
            - Related Resources
            - Related Resources' names
        """
        if not self.where_res:
            filters = ''
            return filters
        else:
            related_labels = self.where_res
            related_res = self.where_res  
            resource = self.related_res_filters

            filters = ''
            for i, res in enumerate(resource):  
                for index, (key, value) in enumerate(res.items()):
                    filters += related_labels[i] + '.' + key + '=' + value + '&'
            filters = filters[:-1]

        return filters

    
    def get_source_res_filters(self):
        """
        Output the URL's part of the API Call that states the filters to be applied on the Source Resource
        """
        # If there is not Where Clause we output an empty string
        if not self.where_res:
            filters = ''
            return filters        
        else:
            where_clause = self.where_res

            related_labels = list(map((lambda x: x + ":"), where_clause))
            related_res = list(map((lambda x: x.capitalize()), where_clause))    

            # The filters to be applied on the Source Resource from the WHERE Clause
            resource = self.source_res_filters
            
            # We initialize filters as an empry string and build it in the following way:
                # The variable (key) + '=' + the value of the filters + & (to link mutliple resources)
            filters = ''
            for i, variable in enumerate(resource):  
                for index, (key, value) in enumerate(variable.items()):
                    filters += key + '=' + value + '&'
                    
            # We remove the last '&'
            filters = filters[:-1]

        return filters
    
    
    def include_res(self):
        """
        Output the URL's part for the API call that states if there are others resources to return
        If there is a JOIN Clause in the query_fileuration file
        """
        
        # Lambda function to flatten a list of list
        flatten = lambda l: [item for sublist in l for item in sublist]
        
        # Return an empty string if there is no JOIN Clause
        try:
            join_res = list(self.join_res.keys())
        except:
            include_parameter = ""
            return include_parameter
        
        join_keys = [list(x.keys()) for x in self.join_res.values()]
        join_keys = flatten(join_keys)
        
        include_syntax = "_include"
        if len(self.query_file["from"]) > 1:
            include_parameter = ''
            for i in range(len(self.query_file["join"])):
                if len(join_keys) > len(join_res):
                    for j in range(len(join_keys)):
                        include_parameter += include_syntax + "=" + join_res[i] + ":" + join_keys[j] + "&"
                else:
                    include_parameter += include_syntax + "=" +  join_res[i] + ":" + join_keys[i] + "&"   
            include_parameter = include_parameter[:-1]

        else:
            include_parameter = ""

        try:
            self.query_file["join_type"]
            include_parameter = include_parameter + '&_revinclude=*'
        except:
            pass

        return include_parameter
    
    
    def query_keywords(self):
        """
        DEPRECATED
        Check if the query syntax is correct and return the query keywords
        """

        # Raise error if the query syntax is wrong
        available_keywords = ['SELECT', 'FROM', 'WHERE', 'JOIN', 'GROUP_BY', 'LIMIT', 'AS', 'JOIN_TYPE']
        
        for key in self.query.keys():
            if key.upper() not in available_keywords:
                print(key)
                raise ValueError('Wrong Syntax')
            
            if 'select' not in self.query.keys():
                raise ValueError('The query must contain the keyword SELECT')
            if 'from' not in self.query.keys():
                raise ValueError('The query must contain the keyword FROM')
                
        keywords = set(self.query.keys())
        
        
    def make_url(self):
        """
        Output the full URL to be sent to the API
        """
        
        base = self.base
        source = self.source_resource  
        source_filters = self.source_res_filters_full
        other_filters = self.related_res_filters_full
        include = self.include_parameter
        
        # Assemble the url's components
        url = base + source + '?' + other_filters + '&' + source_filters + '&' + include

        # Cleaning of extra '&'
        if url[-1] == '&': 
            url = url[:-1]
        url = url.replace('?&', '?')
        url = url.replace('&&', '&')
                        
        return url
















