"""Main module."""
from .FhirQuery import FhirQuery
from .RequestApi import RequestApi
from .ToDataset import ToDataset

class EkiFhir(object):

	def __init__(self, api_url, path_to_query):

		self.api_url = api_url
		self.query = path_to_query

	def	make_url(self):

		url = FhirQuery(self.api_url, self.query)
		url = url.make_url()

		return url

	def request(self):
		"""
		Call the RequestApi class to load the API json's response
		"""

		url = self.make_url()

		# Call RequestApi class
		fhir_json = RequestApi(url)
		fhir_json = fhir_json.response()

		return fhir_json

	def to_dataframe(self, how='left'):
		"""
    	Call the ToDataset class to parse the API json's response into 
    	a Pandas dataframe according to the query specification
		"""

		query = self.query
		response = self.request()

		df = ToDataset(query, response)
		df = df.to_dataframe(how=how)

		return df
