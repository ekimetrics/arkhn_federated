import pandas as pd
import json


class ToDataset(object):
    """
    A class that transform a json FHIR REST API's response into a pandas dataframe
    according to a set of rules specified in a JSON configuration file

    ...

    Attributes
    ----------
    path: str
        The path to the configuration file
        Example of the structure of a JSON config file:
        {
        "select": {
            "Observation": [
            "component.valueQuantity.value"
            ],
            "Patient": [
            "birthDate",
            "gender"
            ]
        },
        "from": {
            "Patient": "patient",
            "Observation": "observation"
        },
        "join": {
            "Observation": {
            "subject": "patient"
            }
        },
        "where": {
            "Patient" : {
            "birthdate": {"ge": "1970"}
            },
            "Observation": {
            "code": "http://loinc.org|55284-4"
            }
        }
        }
    
    
    api_response:
        A nested JSON containing the FHIR API's reponse
        

    Methods
    -------
    build_dataset
        Returns a dataframe from a nested JSON
         - It iteratively join resources if joins are requires
         If a join is impossible is message is printed and a dataframe 
         without this join operation is returned

    """    
    
    def __init__ (self, path, api_response):
        
        self.api_response 				= api_response
        self.path 						= path
        
        self.query 						= self.read_json()
        self.query_keywords 			= self.query_keywords()
        
        self.query_resources 			= self.fetched_resources()[0]
        self.query_resources_name 		= self.fetched_resources()[1]
        self.query_from_resources 		= self.fetched_resources()[3]
        self.query_from_resources_name 	= self.fetched_resources()[3]
        self.query_join_ressources 		= self.fetched_resources()[4]
        self.query_join_ressources_name = self.fetched_resources()[5]
        
        self.query_variables_name 		= self.select_var()[0]
        self.query_variables_root_name  = self.select_var()[1]
        
        self.config_join_keys 			= self.join_keys()[0]
        self.config_join_vars 			= self.join_keys()[1]
        

    def read_json(self):

        with open(self.path) as f:
            file = json.load(f)
        
        return file
           
    def query_keywords(self):
        """
        CURRENTLY NOT USED
        Check if the query syntax is correct and return the query keywords
        """

        # Raise error if the query syntax is wrong
        available_keywords = ['SELECT', 'FROM', 'WHERE', 'JOIN', 'GROUP_BY', 'LIMIT', 'AS', 'JOIN_TYPE']
         
        for key in self.query.keys():
            if key.upper() not in available_keywords:
                raise ValueError('Wrong Syntax')
            
            if 'select' not in self.query.keys():
                raise ValueError('The query must contain the keyword SELECT')
            if 'from' not in self.query.keys():
                raise ValueError('The query must contain the keyword FROM')
                
        keywords = list(self.query.keys())

        return keywords
    
    def read_clause(self, keyword):
        """
        Extract the query's clause specified by the keyword and the related variables
        """
        
        try:
            clause = self.query[keyword]
            clause_vars = list(self.query[keyword].keys())
        except:
            clause = ""
            clause_vars = ""
            
        return clause, clause_vars
    
    def fetched_resources(self):
        """
        Return the name and the content (specified variables) 
        of each resource in the config file
        """
        
        # Variables from SELECT clause
        select_clause, select_vars_name = self.read_clause('select')
        
        # Variables from FROM clause
        from_clause, from_vars_name = self.read_clause('from')
        
        # Variables from JOIN clause
        join_clause, join_vars_name = self.read_clause('join')

        return select_clause, select_vars_name, from_clause, from_vars_name, join_clause, join_vars_name
    
    def select_var(self):
        """
        Read the configuration file and return a list of variables to be extracted from the FHIR API's output
        These include the variables specified in the SELECT and FROM clause
        As the resourceType & id variables are necessary for any join we append them to list returned
        """

        # Append all the variables' names specified in the SELECT clause of the Config File
        select_vars = [var for res in self.query_resources_name for var in self.query_resources[res] ]

        # Append all the variables' names specified in the FROM clause of the Config File
        # if not already specified
        join_vars = ['.'.join([key, val]) for res in self.query_join_ressources for key, val in self.query_join_ressources[res].items() ]

        # Append the variables of the JOIN clause to the variables of the SELECT clause
        # and add 'resourceType' & 'id' in order to perform join if necessary
        vars_list = select_vars + join_vars 
     
        # Takes the fist part the query variable ('.' as separator) if necessary
        # to be able to browse the FHIR JSON file
        vars_root_name_list = [res.split('.')[0] for res in vars_list]
        
        vars_list.append('resourceType')
        vars_list.append('id')
        
        return vars_list, vars_root_name_list
    
    def join_keys(self):
        """
        Return the resources'names to be joined and their join keys
        """

        join_resource_names = self.read_clause("join")[1]
        join_resource_keys = ['.'.join([key, value]) 
                            for resource in self.read_clause("join")[0]
                            for key, value in self.read_clause("join")[0][resource].items() ]
    
        return join_resource_names, join_resource_keys
    

    def flatten(self, dictionary, sep = "."):
        """
        Function to flatten a nested dictionary structure
        :param dictionary: A python dict()
        :param delimiter:  The desired delimiter between 'keys'
        :return: dict()
        :Example input : 
            "use": {
              "coding": [ {
                "system": "http://terminology.hl7.org/CodeSystem/diagnosis-role",
                "code": "AD"]
        => Output : {use.coding.0.system, use.coding.0.code}
        """
        obj = {}

        def recurse(dictionary, parent_key = ""):
            
            # loop through list and call recurse()
            if isinstance(dictionary, list):
                for i in range(len(dictionary)):
                    recurse(dictionary[i], parent_key + sep + str(i) if parent_key else str(i))
            # loop through dictionary and call recurse()
            elif isinstance(dictionary, dict):
                for key, val in dictionary.items():
                    recurse(val, parent_key + sep + key if parent_key else key)
            else:
                # use the parent_key and store the value to obj
                obj[parent_key] = dictionary

        recurse(dictionary)

        return obj
    

    def parse_resource(self, dict_to_parse, multiple_resource=True):
        """
        Transform the API's JSON response into a smaller, usable and flattened dictionnary
        that can be transformed to a dataframe
        
        Input: the API's json response
        Output: a flattened dictionnary with only the desired keys 
                (i.e. those specified in the configuration files)
        """
        vars_name = self.query_variables_name
        vars_root_name = self.query_variables_root_name
        # Create an output dictionnary. 
        output_dict = {}
        
        # Build a new unested dictionnary containing only the desired keys/variables (listed in vars_name)
        # The loop calls the self.flatten function
        for i in range( len(dict_to_parse) ):
           
            output_dict[i] = {key: self.flatten(dict_to_parse[i]['resource'])[key]
                                    for key in self.flatten(dict_to_parse[i]['resource']).keys()
                                    & (vars_name) }
        
            # If there are multiple resource to join we create the key resourceTypeId which is necessary 
            # to perform any join between resources. It is is the combination of the keys resourceType and ID            
            if multiple_resource == True:
                output_dict[i]["resourceTypeId"] = output_dict[i]["resourceType"] + "/" + output_dict[i]["id"]
                if 'id' not in vars_root_name: output_dict[i].pop('id', None)
            
            # Else we remove the 'id' & 'resourceType' keys if they are not specified in the SELECT statement 
            else:
                if 'id' not in vars_root_name: output_dict[i].pop('id', None)
                if 'resourceType' not in vars_root_name: output_dict[i].pop('resourceType', None)         
        
        return output_dict


    def single_res_df(self, resource):
        """
        Transform the JSON Response into a Pandas Dataframe with the desired variables
        lookup_ressource: the resource's name of interest
        """
        api_response = self.api_response['entry']
            
        dict_to_parse = self.parse_resource(api_response, multiple_resource=False)

        # We transform the dictionnary into pandas DataFrames
        df = pd.DataFrame.from_dict(dict_to_parse, orient='index')
        df = df.add_prefix(resource + '.')
        
        return df
        

    def multiple_res_df(self, left_resource, right_resource, counter, df1=None, how='left'):
        """
        Transform the JSON Response into a Pandas Dataframe with the desired variables
        if there are different resources to join together
        left_ressource: the resource 'source' on which we join the others resources
        right_resource: the resource that will be joined to the 'source' resource
        counter: 
        df1
        """
        
        vars_name = self.query_variables_name

        api_response = self.api_response['entry']
        parsed_dict = self.parse_resource(api_response)

        # We split the parsed dictionnary into a dictionnary containing only the looked up resource
        # (the resource on the left of the join) and a dictionnary containing only the targeted resource
        # (the ressource on the right of the join)
        left_res_dict = []
        right_res_dict = []
        for i in range(len(parsed_dict)):

            if parsed_dict[i]['resourceType'] == left_resource.capitalize():
                left_res_dict.append(parsed_dict[i])

            if parsed_dict[i]['resourceType'] == right_resource.capitalize():
                right_res_dict.append(parsed_dict[i])
        
        # The join keys' names
        left_on = left_resource + '.' + self.config_join_vars[counter]
        right_on = right_resource + ".resourceTypeId"
        
        # We transform the two dictionnaries into pandas DataFrames
    
        # If the left dataframe (df1) doesn't exist (i.e. we perform the first join)
        # Else df1 is already passed as an argument of the function
        if df1 is None:
            df1 = pd.json_normalize(left_res_dict, meta = vars_name )
            df1 = df1.add_prefix(left_resource + '.')    
            
        df2 = pd.json_normalize(right_res_dict, meta = vars_name)   
        df2 = df2.add_prefix(right_resource + '.')

        # Merge the DataFrames
        # If the merging fail only the left dataframe is outputed
        try:
            df = self.join_df(df1, df2, left_on=left_on, right_on=right_on, how=how)  
        except:
            print(f"Couldn't join on {right_on}")
            df = df1
    
        return df
              
        
    def join_df(self, df1, df2, left_on, right_on, how='inner'):
        """
        Merge the dataframes is drop columns not specified in the configuration file
        """

        df = pd.merge(df1, df2, 
                      left_on = left_on, 
                      right_on = right_on,
                      how = how )     
        
        # Drop columns which are not the SELECT Statement
        cols_in_select = ['.'.join([key.capitalize(), val]) 
                        for key, value in self.query_resources.items() 
                        for val in self.query_resources[key]   ]

        cols_not_in_select = [elem for elem in df.columns if elem not in cols_in_select]
        df.drop(cols_not_in_select, axis=1, inplace=True)
        
        return df
    

    def to_dataframe(self, how='left'):
        """
        Transform the API's output into a dataframe according 
        to the conditions set in the configuration file
        These conditions included the variables to be extracted
        as well as the resources that needed to be joined together
        """
        # List of the resources from the FROM and JOIN statement
        from_resources_name = self.query_from_resources_name
        resources_to_join = self.config_join_keys
       
        # Case where there are no join operatons to perform
        # i.e. there is no JOIN Statement
        if len(resources_to_join) == 0:
            df = self.single_res_df(resource=from_resources_name[0])
            
        # Case where join operations need to be performed
        # i.e. there is a JOIN Statement
        elif len(resources_to_join) > 0:
            
            # In that case we perform the joins in an iterative fashion
            # resources are successively joined in the order in which they appear 
            # in the JOIN statement
            for i in range(len(resources_to_join)):
                # As there are no intialized dataframe to be joined on during 
                # the first join, we set the parameters df1 to None

                if i == 0:
                    df = self.multiple_res_df(df1 = None,
                                              left_resource = resources_to_join[i], 
                                              right_resource = from_resources_name[i+1], 
                                              counter = i, 
                                              how = how)

                    # We save the resulting dataframe in a temporary one that will be used
                    # to perform the remaining joins
                    temp_df = df
                    
                else:
 
                    df = self.multiple_res_df(df1 = temp_df, 
                                              left_resource = resources_to_join[i], 
                                              right_resource = from_resources_name[i+1], 
                                              counter = i, 
                                              how = how)
                    
                    # We save the resulting dataframe in a temporary one that will be used
                    # to perform the remaining joins
                    temp_df = df
        
        # and reset the dataframe index
        df.reset_index(drop = True, inplace=True)
        
        # Final output
        return df
    
    